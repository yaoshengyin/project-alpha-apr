from calendar import monthcalendar
from datetime import date
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskEditForm, TaskForm
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


def show_my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": my_tasks}
    return render(request, "tasks/my_tasks.html", context)


@login_required
def edit_task(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    if request.method == "POST":
        form = TaskEditForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskEditForm(instance=task)
    context = {"form": form}
    return render(request, "tasks/edit_task.html", context)


@login_required
def confirm_delete(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    if request.method == 'POST':
        task.delete()
        return redirect('show_my_tasks')
    return render(request, 'tasks/confirm_delete.html')


@login_required
def calendar_view(request, year=date.today().year, month=date.today().month):
    cal = monthcalendar(year, month)
    tasks = Task.objects.filter(due_date__year=year, due_date__month=month)
    context = {'calendar': cal, 'tasks': tasks}
    return render(request, 'calendar_view.html', context)
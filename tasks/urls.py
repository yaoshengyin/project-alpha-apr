from django.urls import path
from tasks.views import (
    calendar_view, confirm_delete, 
    create_task, show_my_tasks, edit_task
)

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("edit/<int:task_id>/", edit_task, name="edit_task"),
    path(
        'confirm_delete/<int:task_id>/', confirm_delete, name='confirm_delete'
        ),
    path(
        'calendar/', calendar_view, name='calendar_view'
        ),
]

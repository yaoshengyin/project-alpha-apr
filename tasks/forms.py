from django import forms
from tasks.models import Task


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            "name", 
            "start_date", 
            "due_date", 
            "project", 
            "assignee",
            "time_spent",
            ]
        widgets = {
            'start_date': forms.DateInput(attrs={'type': 'date'}),
            'due_date': forms.DateInput(attrs={'type': 'date'}),
        }


class TaskEditForm(forms.ModelForm):
    time_spent = forms.CharField(help_text="Please use HH:MM:SS format")
    
    class Meta:
        model = Task
        fields = [
            "name", 
            "start_date", 
            "due_date", 
            "project", 
            "assignee", 
            "status",
            "priority",
            "time_spent",
            ]
        widgets = {
            'start_date': forms.DateInput(attrs={'type': 'date'}),
            'due_date': forms.DateInput(attrs={'type': 'date'}),
        }
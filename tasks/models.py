from django.db import models
from projects.models import Project
from django.contrib.auth.models import User

# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    time_spent = models.DurationField(null=True, blank=True)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        User, related_name="tasks", on_delete=models.CASCADE
    )
    PRIORITY_CHOICES = [
        ('high', 'high'),
        ('medium', 'medium'),
        ('low', 'low'),
    ]
    priority = models.CharField(
        max_length=6,
        choices=PRIORITY_CHOICES,
        default='medium',
    )
    STATUS_CHOICES = [
        ('in_progress', 'in_progress'),
        ('completed', 'completed'),
        ('paused', 'paused'),
    ]
    status = models.CharField(
        max_length=12,
        choices=STATUS_CHOICES,
        default='in_progress',
    )

    def __str__(self):
        return self.name
